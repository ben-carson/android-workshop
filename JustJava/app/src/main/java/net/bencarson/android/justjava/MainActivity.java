package net.bencarson.android.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    int priceOfCoffee = 5;
    int quantity = 2;
    private static int chocoPrice = 2;
    private static int whipPrice = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateQuantityOnView(quantity);
    }

    public void incrementQuantity(View view) {
        if(quantity < 101) {
            quantity = quantity + 1;
            updateQuantityOnView(quantity);
        } else {
            Toast.makeText(this, "You cannot order more than 100 coffees.", Toast.LENGTH_SHORT).show();
        }
    }
    public void decrementQuantity(View view) {
        if(quantity > 1) {
            quantity = quantity - 1;
            updateQuantityOnView(quantity);
        } else {
            Toast.makeText(this, "You cannot order less than 1 coffee.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        int total = priceOfCoffee*quantity;
        String customerName = ((EditText)findViewById(R.id.input_user_name)).getText().toString();

        StringBuilder strBr = new StringBuilder();
        strBr.append(getString(R.string.lbl_customer_name) + " ");

        strBr.append(customerName + "\n");

        strBr.append(getString(R.string.lbl_whip_cream_purchase) + " ");
        boolean addWhip = ((CheckBox) findViewById(R.id.whip_cream_checkbox)).isChecked();
        strBr.append(addWhip + "\n");
        if(addWhip) {
            total += (whipPrice*quantity);
        }

        strBr.append(getString(R.string.lbl_choc_sauce_purchase) + " ");
        boolean addChoc = ((CheckBox) findViewById(R.id.chocolate_checkbox)).isChecked();
        strBr.append(addChoc + "\n");
        if(addChoc) {
            total += (chocoPrice*quantity);
        }

        strBr.append(getString(R.string.lbl_purchase_quantity) + " ");
        strBr.append(quantity + "\n");


        strBr.append(getString(R.string.lbl_purchase_total_price) + " ");
        strBr.append(NumberFormat.getCurrencyInstance().format(total) + "\n");

        strBr.append(getString(R.string.purchase_gratitude));

        Intent emailIntent = new Intent();
        emailIntent
            .setData(Uri.parse("mailto:")) //ensure only mail apps work on this intent
            .setAction(Intent.ACTION_SENDTO)
            .putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@catsandcode.com"})
            .putExtra(Intent.EXTRA_SUBJECT, "\"Just Java\" Coffee Order")
            .putExtra(Intent.EXTRA_TEXT, strBr.toString());

        //Ensure the system has an app to handle this intent
        if(emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(emailIntent);
        } else {
            Toast.makeText(this, "There is no app capable of handling this request on this system.", Toast.LENGTH_SHORT);
        }
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void updateQuantityOnView(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }
}