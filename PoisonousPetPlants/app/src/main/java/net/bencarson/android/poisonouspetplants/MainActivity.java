package net.bencarson.android.poisonouspetplants;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    Set poisonList = new HashSet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //fill the list with temp values, until service is hooked up
        initPList();

        final Button checkItemBtn = (Button) findViewById(R.id.btn_check_item);
        checkItemBtn.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText userInputView = (EditText)findViewById(R.id.edit_food_item);
                    String itemToCheck = userInputView.getText().toString().trim();
                    boolean trouble = isPoison(itemToCheck);
                    //convert toast to fragment
                    Toast.makeText(v.getContext(), itemToCheck + ": " + trouble, Toast.LENGTH_SHORT).show();
                }
            }

        );
    }

    private void initPList() {
        if(poisonList.isEmpty() ) {
            poisonList.add("poinsettia");
            poisonList.add("hazelnut");
            poisonList.add("lily");
            poisonList.add("chocolate");
            poisonList.add("onion");
            poisonList.add("grape");
            poisonList.add("raisin");
            poisonList.add("xylitol");
        }
    }

    private boolean isPoison(String str) {
        //remember this, as all db entries will have to be lowercase to work
        return poisonList.contains(str.toLowerCase());
    }
}
