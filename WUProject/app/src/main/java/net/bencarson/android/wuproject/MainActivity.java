package net.bencarson.android.wuproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.http.Url;

public class MainActivity extends AppCompatActivity {

    OkHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            Button forecastBtn = new Button(getApplicationContext());
            forecastBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tempForecast = getForecast();

                }
            });

        } catch (IOException ioe) {
            Toast.makeText(getApplicationContext(),ioe.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    private String createUrlString() {
        StringBuilder str = new StringBuilder();
        str.append(getString(R.string.wu_api_url));
        str.append(getString(R.string.wu_api_key));
        str.append(getString(R.string.wu_loc_svc));
        return str.toString();
    }

    private OkHttpClient getClient() {
        if(client == null) {
            client = new OkHttpClient();
        }
        return client;
    }

    private String getForecast() throws IOException {
        Request req = new Request.Builder()
                .url(createUrlString())
                .build();
        Response resp = getClient().newCall(req).execute();
        JsonParser parser = new JsonParser();
        parser.parse(resp.body().string());
    }
}
