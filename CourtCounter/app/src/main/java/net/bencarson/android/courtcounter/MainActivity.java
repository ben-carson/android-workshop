package net.bencarson.android.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int scoreTeamA = 0;
    private int scoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button threePointBtn = (Button) findViewById(R.id.threePointBtn);
        Button twoPointBtn = (Button) findViewById(R.id.twoPointBtn);
        Button freeThrowBtn = (Button) findViewById(R.id.freeThrowBtn);

        Button threeForB = (Button) findViewById(R.id.threePointBtnForTeamB);
        Button twoForB = (Button) findViewById(R.id.twoPointBtnForTeamB);
        Button freeForB = (Button) findViewById(R.id.freeThrowBtnForTeamB);

        Button resetBtn = (Button) findViewById(R.id.resetScoreBtn);

        threePointBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                scoreTeamA += 3;
                displayTeamAScore(scoreTeamA);
            }
        });

        twoPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreTeamA += 2;
                displayTeamAScore(scoreTeamA);
            }
        });

        freeThrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreTeamA++;
                displayTeamAScore(scoreTeamA);
            }
        });

        threeForB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreTeamB += 3;
                displayForTeamB(scoreTeamB);
            }
        });

        twoForB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scoreTeamB += 2;
                displayForTeamB(scoreTeamB);
            }
        });

        freeForB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayForTeamB(++scoreTeamB);
            }
        });

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetScore();
            }
        });
    }

    public void displayTeamAScore(int score) {
        TextView teamAScore = (TextView) findViewById(R.id.team_a_score);
        teamAScore.setText(String.valueOf(score));
    }

    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    public void resetScore() {
        scoreTeamA = scoreTeamB = 0;
        displayTeamAScore(scoreTeamA);
        displayForTeamB(scoreTeamB);
    }
}
