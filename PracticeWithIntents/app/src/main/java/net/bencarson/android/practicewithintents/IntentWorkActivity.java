package net.bencarson.android.practicewithintents;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.net.URI;

public class IntentWorkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_work);

        final Button intentButton = (Button) findViewById(R.id.btn_intent_action);
        intentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(intentButton.getContext(), "Activating Intent", Toast.LENGTH_SHORT).show();
                Intent testIntent = new Intent(Intent.ACTION_VIEW);
                testIntent.setData(Uri.parse("geo:30.268720,-97.742826"));
                if(testIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(testIntent);
                }
            }
        });
    }
}
