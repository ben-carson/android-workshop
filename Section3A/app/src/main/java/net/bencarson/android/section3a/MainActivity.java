package net.bencarson.android.section3a;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((TextView)findViewById(R.id.status_text_view)).setText(R.string.before_cookie);

        Button eatBtn = (Button) findViewById(R.id.eat_button);
        eatBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                eatCookie(view);
            }

        });

    }

    /**
     * Called when the cookie should be eaten.
     */
    public void eatCookie(View view) {
        // TODO: Find a reference to the ImageView in the layout. Change the image.
        ImageView imageArea = (ImageView) findViewById(R.id.android_cookie_image_view);
        imageArea.setImageResource(R.drawable.after_cookie);
        // TODO: Find a reference to the TextView in the layout. Change the text.
        TextView textArea = (TextView) findViewById(R.id.status_text_view);
        Log.i(R.string.app_name + " Pre-eating: ", textArea.getText().toString());
        textArea.setText(R.string.after_cookie);
        Log.i(R.string.app_name + " Post-eating: ", textArea.getText().toString());
    }
}